voice = new Wad
  source: 'mic'
  reverb:
    wet: .1
    impulse: 'audio/impulse.wav'

instrumental = new Wad
  source: '/audio/xxplosive.mp3'

mixer = new Wad.Poly
  recConfig:
    workerPath: 'js/vendor/recorder_worker.js'
  compressor:
    attack: .003
    knee: 30
    ratio: 12
    release: .25
    threshold: -24

mixer.add(voice)
mixer.add(instrumental)

$('body').on 'click', '.record', ->
  if mixer.rec then mixer.rec.clear()
  mixer.rec.record()
  voice.play()
  instrumental.play()
  $(@).removeClass('record').addClass('stop').text('Stop')

$('body').on 'click', '.stop', ->
  voice.stop()
  instrumental.stop()
  mixer.rec.stop()
  mixer.rec.createWad()
  $(@).removeClass('stop').addClass('record').text('Record')
  $('.actions').show()

$('body').on 'click', '.playback', ->
  mixer.rec.recordings[0].play()

$('body').on 'click', '.upload', ->
  mixer.rec.exportWAV (blob) ->
    fd = new FormData
    fd.append("recording", blob)
    fd.append("email", "jeff@carrotcreative.com")

    $.ajax
      type: 'POST'
      url: 'http://localhost:9292/upload'
      data: fd
      cache: false
      contentType: false
      processData: false
      success: (res) -> log('success', res)
      error: (res) -> log('error', res)

log = (type, msg) ->
  $('.console').css(display: 'inline-block').addClass(type).text(JSON.stringify(msg))
