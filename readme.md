# raybans-booth-prototype

Prototyping audio recording and such

### Setup

- make sure [node.js](http://nodejs.org) and [roots](http://roots.cx) are installed
- clone this repo down and `cd` into the folder
- run `npm install`
- run `roots watch`
- ???
- get money

### Soundcloud Upload

To get the soundcloud upload working, you need to also be running the server. You can clone it down [here](https://github.com/carrot/raybans-booth-server), and follow the setup instructions. If it is running, the soundcloud upload will work.

### Deploying

- If you just want to compile the production build, run `roots compile -e production` and it will build to public.
- To deploy your site with a single command, run `roots deploy -to XXX` with `XXX` being whichever [ship](https://github.com/carrot/ship#usage) deployer you want to use.
